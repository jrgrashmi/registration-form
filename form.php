<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>wordpress form</title>
</head>
<style>
    body {

        margin: 0;
        padding: 2rem;
    }

    .form-outer {
        border: .01rem solid #000;
        padding: 1rem;
    }
</style>

<body>
    <h2 class="text-center"> Registration Form </h2>
    <section class="container form-outer">

        <form class="row g-3" action="process.php" method="post">
            <div class="col-md-6">
                <label for="fname" class="form-label"> First Name</label>
                <input type="text" class="form-control" name="fname" id="fname" required>
            </div>
            <div class="col-md-6">
                <label for="lname" class="form-label">Last Name</label>
                <input type="text" class="form-control" name="lname" id="lname" required>
            </div>
            <div class="col-12">
                <label for="address1" class="form-label">Street Address</label>
                <input type="text" class="form-control" name="address1" id="address1" placeholder="House number and street name" required>
            </div>
            <div class="col-12">
                <input type="text" class="form-control" name="address2" id="address2" placeholder="Apartment, suite, unit, etc, (Optional)" required>
            </div>
            <div class="col-md-6">
                <label for="city" class="form-label">City</label>
                <input type="text" class="form-control" name="city" id="city" required>
            </div>
            <div class="col-md-6">
                <label for="pin" class="form-label">Pin</label>
                <input type="text" class="form-control" name="pin" id="pin" required>
            </div>
            <div class="mb-3">
                <label for="email" class="form-label">Email address</label>
                <input type="email" class="form-control" name="email" id="email" required>
            </div>
            <div class="col-12">
                <label for="phone" class="form-label">Phone</label>
                <input type="tel" class="form-control" name="phone" id="phone" required>
            </div>
            <div class="col-12">
                <label for="amount" class="form-label">Amount</label>
                <input type="text" class="form-control" name="amount" id="amount" required>
            </div>
            <div class="col-12">
                <button type="submit" class="btn btn-primary" name="submit">Submit</button>
            </div>
        </form>
    </section>

</body>

</html>